class City:
    """
    A class representation of a City

    Attributes:
    city_name : str
        The name of the city
    """

    def __init__(self, city_name):
        """
        Constructor for the City class

        Parameters:
        city_name : str
            The name of the city
        """

        # Note: Other properties could be added to the city class such as:
        #    elevation, population, area, latitude and longitude
        self.city_name = city_name