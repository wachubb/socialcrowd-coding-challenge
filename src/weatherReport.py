from src.city import City

class WeatherReport:
    """
    A class representation of a Weather Report

    Attributes:
    city : obj
        The object that represents a city
    wind_speed: int
        The value of the windspeed for the report
    temperature: int
        The value of the temperature for the report
    humidity: int
        The value of the humidity for the report
    """

    def __init__(self, city, wind_speed, temperature, humidity):
        """
        The constructor for the WeatherReport class

        Parameters:
        city : obj
            The object that represents a city
        wind_speed: int
            The value of the windspeed for the report
        temperature: int
            The value of the temperature for the report
        humidity: int
            The value of the humidity for the report
        """

        self.city = city
        self.windSpeed = wind_speed
        self.temperature = temperature
        self.humidity = humidity