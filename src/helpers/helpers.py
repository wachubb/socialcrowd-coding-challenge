""" Helpers.py  This module is used for different helper functions for the main program. """

import random
import math
import yaml
import sys

def randomArraySelection(input_array):
    """
    Randomly selects an item from the array provided.

    Parameters:
    input_array : []
        a single dimension array
    
    Return: Randomly selected value from the array
    """

    randomNum = math.floor(random.randrange(1, len(input_array)))
    return input_array[randomNum]

def randomRangeSelection(min_value, max_value):
    """
    Randomly selects an a value within the provided min value and max value.

    Parameters:
    min_value : int
        a minimum integer value for the range
    max_value : int
        a maximum integer value for the range
    
    Return: Randomly selected integer
    """

    randomNum = math.floor(random.randrange(min_value, max_value))
    return randomNum

def yamlReader(file_name):
    """
    Reads the contents of the yaml file provided and returns them.

    Parameters:
    file_name : str
        The path tho the filename that will be read
    
    Return: If read is successful, returns yaml contents
    """

    # Safely read from the yaml file
    with open(file_name, "r") as stream:
        try:
            return yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
            sys.exit(0)

def promptUserFromArray(array_options, base_user_prompt):
    """
    Prompt the user for input using options from an array. Make sure the selection exists

    Parameters:
    array_options : []
        Array of options for user to choose from
    base_user_prompt: str
        The string to start the user prompt with
    
    Return: index of the option chosen
    """

    user_selection = ''
    input_prompt = (base_user_prompt + "\n") + "\n".join([f'{index + 1}  {item}' for index, item in enumerate(array_options)]) + "\n"
    while user_selection not in map(str, range(1, len(array_options) + 1)):
        user_selection = input(input_prompt)
    
    return user_selection

def getYamlKeys(inputObject):
    """
    Prompt the user for input using options from an array. Make sure the selection exists

    Parameters
    ----------
    input_object : obj
        YAML object to retrieve keys from
        
    Return: Array of the keys from the object
    """
    return list(inputObject.keys())