This is the readme file for this Python exercise. The following outlines the general instructions:

Data Generation: The application should generate random weather data for different cities. Each data point should include temperature, humidity, and wind speed.

OOP Design: Implement classes and/or modules to represent different aspects of the weather data (e.g. a `City`, or `WeatherReport`)

Configuration: Use a YAML file to configure the list of cities and any other parameters (like the range of possible temperature values)

Output: The processed data should be displayed in `JSON` format on the console

Interactivity: Allow the user to specify a city and date range for which they want to see the weather report

Documentation: Write clear comments and documentation for your code


Some additional thoughts:
    I have built this app to essentially give the user the choice of city and then randomly generate all of the rest of the weather data.
        In order to handle user date selection, it would create a list of WeatherReport objects based on the number of days selected.
    
    I also added in the option for the user to select different measurement scales for temperature and speed.

    This has been a pretty fun small project so far. I've really enjoyed this.