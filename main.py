from src.city import City
from src.helpers.helpers import getYamlKeys, promptUserFromArray, yamlReader, randomRangeSelection
from src.weatherReport import WeatherReport
import json

# Read the yaml config file
yamlContents = yamlReader("./dataConfig.yaml")

# Prompt the user for the city. Make sure the selection exists
cities = yamlContents['cities']
user_selected_city = promptUserFromArray(cities, "Please select a city option from the following:")

# Prompt the user for the temperature scale to use. Make sure the selection exists
temperature_options = getYamlKeys(yamlContents['temperatureRange'])
user_selected_temperature = promptUserFromArray(temperature_options, "Please select the temperature scale from the following:")
selected_temp_range = yamlContents['temperatureRange'][temperature_options[int(user_selected_temperature) - 1]]

# Prompt the user for the speed scale to use. Make sure the selection exists
wind_speed_options = getYamlKeys(yamlContents['windSpeed'])
user_selected_wind_speed = promptUserFromArray(wind_speed_options, "Please select the wind speed scale from the following:")
selected_wind_speed_range = yamlContents['windSpeed'][wind_speed_options[int(user_selected_wind_speed) - 1]]

# TODO: Add in user prompt to select a date or range of dates using options from the config. Once a selection has been made,
    # iterate over each day and generate a weather report to be printed out

# Generate all of the randomized data
temperature = randomRangeSelection(selected_temp_range[0], selected_temp_range[1])
wind_speed = randomRangeSelection(selected_wind_speed_range[0], selected_wind_speed_range[1])
humidity = randomRangeSelection(yamlContents['humidity'][0], yamlContents['humidity'][1])

# Create the City and WeatherReport objects
selectedCity = City(cities[int(user_selected_city) - 1])
weatherReport = WeatherReport(selectedCity.__dict__, wind_speed, temperature, humidity)

print(json.dumps(weatherReport.__dict__))